fun printMessage(message: String): Unit {
    println(message)
}
fun printMessageWithPrefix(message: String, prefix: String = "Info") {
    println("[$prefix] $message")
}
fun sum(x:Int, y: Int): Int {
    return x + y
}
fun multiply(x: Int, y: Int) = x * y

fun main() {
    printMessage("Hell-O")
    printMessageWithPrefix("Hello","Hell-O")
    printMessageWithPrefix("Hello")
    printMessageWithPrefix(prefix = "Hell-O", message = "Hello")
    println(sum(1,2))
}