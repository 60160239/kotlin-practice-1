fun eatACake() = println("Eat cake")
fun bakeACake() = println("Bake cake")

fun main() {
    var cakesEaten = 0
    var cakesBaked = 0

    while (cakesEaten < 5) {
        eatACake()
        cakesEaten ++
    }
    do {
        bakeACake()
        cakesBaked++
    }while (cakesBaked < cakesEaten)

    val cakes = listOf("carrot","cheese","chocolate")
    for (cake in cakes) {
        println("It's a $cake cake!")
    }
}