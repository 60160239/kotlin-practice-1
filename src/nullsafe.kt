fun main() {
    var neverNull: String = "This can not be null"
    //neverNull = null
    var nullable: String? = "You can keep a null here"
    nullable = null
    var inferredNonNull = "The complier assumes non-null"
    //inferredNonNull = null

    fun strLength(notNull: String): Int {
        return notNull.length
    }

    strLength(neverNull)
    //strLength(nullable)

    fun describeString(maybeString: String?): String {
        if (maybeString != null && maybeString.length > 0) {
            return "String of length ${maybeString.length}"
        } else {
            return "Empty"
        }
    }

    println(describeString(""))
    println(describeString("Krone"))
}