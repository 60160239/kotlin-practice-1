fun main() {
    val authors = setOf("A","B","C")
    val writers = setOf("B","C","A")

    println(authors == writers)
    println(authors === writers)
}