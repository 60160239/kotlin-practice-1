fun main() {
    cases("Hello")
    cases(1)
    cases(0L)
    cases(MyClass())
    cases("hello")
}

fun cases(obj: Any) {
    when (obj) {
        1 -> println("one")
        "Hello" -> println("greetings")
        is Long -> println("long Long")
        !is String -> println("Not String")
        else -> println("unknown")
    }
}

class MyClass