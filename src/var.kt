fun main() {
    var a: String = "init"
    println(a)
    val b: Int = 1
    val c = 3
    println(b)
    println(c)

    //var e: Int
    //println(e)

    val d: Int
    if (b==1) {
        d = 1
    } else {
        d = 2
    }
    println(d)
}